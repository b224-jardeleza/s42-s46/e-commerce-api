const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Register User
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 12)
	})

	// Check for any duplicate
	return User.findOne({email: reqBody.email}).then(result => {

		if(result){
			
			return false
		
		} else {

			// continue to register user if do not exist
			return newUser.save().then((user, error) => {

				if(error){
					return false
				} else {
					return true
				}
			})

		}
	})	
};


// Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result, error) => {

		if(error){

			return false

		} else {

			if(result == null){

				return false

			} else {

				const isPassWordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPassWordCorrect) {

					return {access: auth.createAccessToken(result)}

				} else {

					return false
				}
			}
		}
		
	})
};


// Get User Profile
module.exports.getProfile = (reqBody) => {
	
	return User.findById(reqBody.id).then((result, error) => {

		if(error){
			
			return false

		} else {

			if(result == null){
				return false

			} else {
				
				result.password = "************"
				return result

			}
		}
	})
};


// Assign User Admin
module.exports.assignAdmin = (reqBody) => {

	let setAdmin = {
		isAdmin: true
	}

	// let setAdminLevel = {
	// 	adminLevel: reqBody.adminLevel
	// }

	return User.findOne({email: reqBody.email}).then(result => {

		if(result.isAdmin){

			return false
		} else {

			return User.findByIdAndUpdate(result._id, setAdmin).then((setUserAdmin, error) => {

				if(error){

					return false
				} else {

					return true
				}
			})
		}
	})
};


// Remove User Admin
module.exports.removeAdmin = (reqBody) => {

	let removedAdmin = {
		isAdmin: false
	}

	return User.findOne({email: reqBody.email}).then(result => {

		if(result.isAdmin){

			return User.findByIdAndUpdate(result._id, removedAdmin).then((removedAdmin, error) => {

				if(error) {

					return false
				} else {

					return true
				}
			})
		} else {

			return false
		}
	})
};


// Add Customer Order
module.exports.addOrder = (data, reqBody) => {

	return Product.findById(data.productId).then(result => {

		let newOrder = new Order({
			userId: data.userId,
			products: [{
				productId: data.productId,
				quantity: data.quantity,
				subtotal: data.quantity * result.unitPrice
			}]
		})

		return Order.findOne({userId: data.userId, isCheckedOut: false}).then(orders => {

			if(orders){

				return Order.findById(orders._id).then(addOrder => {

					addOrder.products.push({
						productId: data.productId,
						quantity: data.quantity,
						subtotal: data.quantity * result.unitPrice
					})

					return addOrder.save().then((addOrder, error) => {

						if(error){

							return false
						} else {

							return true
						}
					})
				});

				return true
			} else {

				return newOrder.save().then((newOrder, error) => {

					if(error){

						return false
					} else {

						return true
					}
				})
			}
		})
	})	
};

// Get User Order
module.exports.getUserOrder = (data) => {
	
	return Order.find({userId: data.userId}).then((result, error) => {

		if(error){
			
			return false

		} else {

			return result
		}
	})
};

// Check Out User's Order
module.exports.checkOut = (data, reqBody) => {

	let check = {
		isCheckedOut: reqBody.isCheckedOut
	}
 	
 	return Order.findOne({userId:data.userId, _id: data.orderId}).then(order => {

 		if(order) {

 			// return order
 			// return Order.findById(order._id).then(result => {

 				// if(result._id === data.orderId) {

 					let totalSum = 0

 					for (let i = 0; i < order.products.length; i++){

 						totalSum += order.products[i].subtotal
 					}

 					let total = {
 						totalAmount: totalSum
 					}

 					return Order.findByIdAndUpdate(data.orderId, total).then((updateOrder, error) => {
		
 						if(error){

 							return false
 						} else {

 							return Order.findByIdAndUpdate(data.orderId, check).then((check, error) => {
 								if(error){

 									return false
 								} else {
 									return true
 								}
 							}) 
 							
 						}
 					})
 				// }
 				//  else {

 				// 	return 'a'
 				// }

 			// })
 		} else {

 			return false
 		}
 	})	
};


// Get all orders (admin access)
module.exports.allOrders = () => {

	return Order.find({}).then((result, error) => {

		if(error){

			return false
		} else {

			return result
		}
	})
};