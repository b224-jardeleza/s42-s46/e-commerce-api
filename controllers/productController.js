const Product = require("../models/Product");
const User = require("../models/User");


// Add item to products collection
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		productName: reqBody.productName,
		productDescription: reqBody.productDescription,
		unitPrice: reqBody.unitPrice,
		category: reqBody.category,
		productImage: reqBody.productImage
	});

	return Product.findOne({productName: reqBody.productName}).then(result => {

		if(result){

			return false

		} else {

			return newProduct.save().then((product, error) => {

				if(error){
					
					return false

				} else {

					return true
				}
			})

		}

	})

};


// Get active products only
module.exports.getActiveProducts = () => {

	return Product.find({isActive: true}).then((result, error) => {
		
		if(error){

			return false
		} else {

			return result
		}
	})
};


// Get ALL products
module.exports.getAllProducts = () => {

	return Product.find({}).then((result, error) => {

		if(error){

			return false
		} else {

			return result
		}

	})
};


// Get specific product
module.exports.getProduct = (reqParams) =>{

	return Product.findById(reqParams.productId).then((result, error) => {

		if(error){

			return false
		} else {

			return result
		}
	})
};


// Update product
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		productName: reqBody.productName,
		productDescription: reqBody.productDescription,
		unitPrice: reqBody.unitPrice,
		category: reqBody.category,
		productImage: reqBody.productImage
	}


	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {

		if(error){

			return false
		} else {

			return true
		}
	})
};


// Archive product
module.exports.archiveProduct = (reqParams, reqBody) => {

	let archivedProduct = {
		isActive: reqBody.isActive
	}

	// let unarchivedProduct = {
	// 	isActive: true
	// }

	// return Product.findById(reqParams.productId).then(result => {

	// 	if(result.isActive){
			return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error) => {

				if(error){

					return false
				} else {
					return true
				}	

			})
		// } else {

		// 	return Product.findByIdAndUpdate(reqParams.productId, unarchivedProduct).then((unarchivedProduct, error) => {

		// 		if(error){

		// 			return false
		// 		} else {

		// 			return false
		// 		}
		// 	})
		// }

}