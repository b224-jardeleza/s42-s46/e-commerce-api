//  Server Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
// const orderRoutes = require("./routes/orderRoutes");

const app = express();
const port = process.env.PORT || 9090;

mongoose.set("strictQuery", false);

// Mongoose Connection || DB Connection Setup
mongoose.connect("mongodb+srv://jardeleza-224:admin123@batch224-jardeleza.gaqgipf.mongodb.net/s42-s46?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

let db = mongoose.connection;

db.on("error", () => console.log("Connection Error"));
db.once("open", () => console.log("Connected to MongoDB"));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);
// app.use("/order", orderRoutes);


app.listen(port, () => {console.log(`API is now running at localhost port: ${port}`)});