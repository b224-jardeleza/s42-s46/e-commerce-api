const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product Name is required!"]
	},
	productDescription: {
		type: String,
		required: [true, "Product Description is required!"]
	},
	unitPrice: {
		type: Number,
		required: [true, "Unit Price is required!"]
	},
	category: {
		type: String,
		required: [true, "Category is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	productImage: {
		type: String,
		default: "https://www.och-lco.ca/wp-content/uploads/2015/07/unavailable-image.jpg"
	},
	addedDate: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Product", productSchema);