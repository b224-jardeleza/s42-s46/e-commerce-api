const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},
	products: [
		{	
			productId: {
				type: String,
				required: [true, "Product ID is required!"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required!"],
				min: 1,
				default: 1
			},
			subtotal: {
				type: Number,
				required: [true, "Subtotal is required"],
				default: 0 
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required!"],
		default: 0
	},
	isCheckedOut: {
		type: Boolean,
		required: [true, "Order Status is required!"],
		default: false
	},
	purchasedOn: {
		type: Date,
		default: new Date
	}

});

module.exports = mongoose.model("Order", orderSchema);