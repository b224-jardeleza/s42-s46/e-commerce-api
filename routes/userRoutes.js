const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user profile
router.get("/profile", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData){		
		userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
});


// Route for setting user as admin (Admin Access)
router.put("/assign-admin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			userController.assignAdmin(req.body).then(resultFromController => res.send(resultFromController))
	} else {		
		res.send("Unauthorized access!")
	}
});


// Route for removing user as admin (Admin Access)
router.put("/remove-admin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			userController.removeAdmin(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Unauthorized access!")
	}
});


// Route for customer order
router.post("/order-product", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		productId: req.body.productId,
		quantity: req.body.quantity	
	}

	if(userData.isAdmin){

		res.send("You are not a customer account.")
	} else {

		userController.addOrder(data).then(resultFromController => res.send(resultFromController))	
	}
	
});

// Route for user profile
router.get("/orders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		orderId: req.params.orderId,
		userId: userData.id
	}

	userController.getUserOrder(data).then(resultFromController => res.send(resultFromController))
});

// Route for check out
router.put("/checkout/:orderId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// if(userData.userId != req.params.userId){

		// res.send("This is not your cart!")
	// } else {

	let data = {
		orderId: req.params.orderId,
		userId: userData.id
	}

		userController.checkOut(data, req.body).then(resultFromController => res.send(resultFromController))
	// }

});


// Route for getting all orders (admin access)
router.get("/orders/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

		userController.allOrders(userData).then(resultFromController => res.send(resultFromController))
	} else {

		res.send("Unauthorized access!")
	}
});


module.exports = router;