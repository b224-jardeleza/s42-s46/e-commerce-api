const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for adding product
router.post("/add-product", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {

		res.send("Unauthorized access!")

	}
});


// Route for getting active ONLY products 
router.get("/", (req, res) => {

	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});


// Route for getting ALL products
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
	} else {

		res.send("Unauthorized access!")
	}
});


// Route for getting specific product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});


// Route for updating product
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {

		res.send("Unauthorized access!")
	}
	
});


// Route for archiving product
router.put("/archive/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){

			productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {

		res.send("Unauthorized access!")
	}
});








module.exports = router;